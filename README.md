SodyoSDK
========
** Get the SodyoSdkExample app from [here](https://bitbucket.org/sodyodevteam/sodyosdkexample-android.git). **


Requirements
-------------------
Before you begin, please make sure that minSdkVersion >= 16 and targetSdkVersion <= 25 (in your \app\build.gradle file) 


Installation
-------------------
* In your project build.gradle file the following (gradle 2.14.1):

```java  
allprojects {
	repositories {
		maven {
			credentials {
				//Bitbucket  credentials
				username sodyoltd@gmail.com
				password sodyo@sdk
			}
			authentication {
				basic(BasicAuthentication)
			}
			url "https://api.bitbucket.org/2.0/repositories/sodyodevteam/sodyosdk-android/src/releases"
		}
		jcenter()

	}

}
```

* In your app build.gradle file add the following:

```java  
android {
 ...
	  packagingOptions {
		exclude "META-INF/DEPENDENCIES.txt"
		exclude "META-INF/LICENSE.txt"
		exclude "META-INF/NOTICE.txt"
		exclude "META-INF/NOTICE"
		exclude "META-INF/LICENSE"
		exclude "META-INF/DEPENDENCIES"
		exclude "META-INF/notice.txt"
		exclude "META-INF/license.txt"
		exclude "META-INF/dependencies.txt"
		exclude "META-INF/LGPL2.1"
		exclude "META-INF/ASL2.0"
		exclude "META-INF/maven/com.google.guava/guava/pom.properties"
		exclude "META-INF/maven/com.google.guava/guava/pom.xml"
	   }

} 

dependencies {
   ...
   compile "com.sodyo:sodyo-android-sdk:3.42.06"
}
	
```

Initialization
-------------------
Prior to launching the Sodyo Scanner, the app should initialize the Sodyo engine. 
This will instantiate the Sodyo SDK and load your app data that is binded with your Sodyo Markers.
The initialization method should be from your Main activity class.


* Call the initialization method from your main activity `onCreate()`:

```java
import com.sodyo.sdk.Sodyo;

	@Override
	public void onCreate() {
		// init Sodyo engine App
		Sodyo.init(application, SODYO_APP_KEY, initializationCallback);
		
		// define a detection callback
		Sodyo.getInstance().setSodyoScannerCallback(this);
		...
		super.onCreate();
	}
```
**Important Note:** The SODYO_APP_KEY can be found in the application provider module of your account in the Sodyo Portal

* Make your activity implement the `SodyoInitCallback` interface to get notified on the result of the initialization process:
```java
import com.sodyo.sdk.SodyoInitCallback;

public void onSodyoAppLoadSuccess();
public void onSodyoAppLoadFailed(String error);
```


Sodyo Marker Detection
-------------------------
The app invokes the Sodyo Scanner to start detecting markers. Sodyo scans for Markers in the camera field of view and uses a callback procedure for each detected Marker.
The Sodyo Scanner will keep running until it is finished.


* Set a callback object that implements ```SodyoScannerCallback``` and receives the scanning results. 

```java
import com.sodyo.sdk.SodyoScannerCallback;

Sodyo.getInstance().setSodyoScannerCallback(this);

Make your activity implement the SodyoScannerCallback interface to get notified on marker scan:

@Override
public void onMarkerDetect(String markerType, String data,String error) {

}
```



* Launch ```SodyoScanner``` Activity using ```startActivityForResult(Intent, int)```. For example:

```java
import com.sodyo.app.SodyoScanner;

private static final int SODYO_REQUEST_CODE = 1111;

Intent intent = new Intent(MainActivity.this, SodyoScanner.class);
startActivityForResult(intent, SODYO_REQUEST_CODE);
```


* When ready, dismiss the ```SodyoScanner``` activity using ```finishActivity(int requestCode)```. Use the request code that was used in the previous section. For example:
```java
finishActivity(SODYO_REQUEST_CODE);
```

Intent Filter
----------------
Add additional intent filter for application to handle sodyo actions:

```xml
            <intent-filter>
                <action android:name="android.intent.action.VIEW"/>

                <category android:name="android.intent.category.DEFAULT"/>
                <category android:name="android.intent.category.BROWSABLE"/>

                <data android:scheme="sodyo"/>

            </intent-filter>
```

## Setting Up ProGuard ##
Add the following lines to your project�s proguard-rules.pro file:
```gradle
-dontwarn com.sodyo.**
-keep class com.crittercism.**
-keepclassmembers class com.com.sodyo.** { *; }
```

## Migrating from previous versions of the SDK ##
To upgrade from previous versions of the SDK, make sure to pull the latest version of the SDK as indicated in the dependencies of the app build.gradle file above.
In addition, change your init function as indicated in the initialization section above.


Authors
-----------------------------------
Ron Yagur, ron@sodyo.com  
Michael Yavorovsky, michael@brainway.co.il


License
------------------------
SodyoSDK is available under [Sodyo License Agreement](https://bitbucket.org/sodyodevteam/sodyosdk-android/raw/HEAD/SodyoLicenseAgreement.pdf).
